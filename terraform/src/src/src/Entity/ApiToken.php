<?php

namespace App\Entity;

use App\Repository\ApiTokenRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ApiTokenRepository::class)
 */
class ApiToken
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * @ORM\Column(type="datetime")
     */
    private $expriteAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="apiTokens", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $userid;

    public function __construct(User $user)
    {
        $this->token = bin2hex(random_bytes(60));
        $this->userid = $user;
        $this->expriteAt = new DateTime('+1 hour');
    }

    public function isExpired()
    {       
        return $this->getExpriteAt() <= new DateTime();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(User $user)
    {
        $this->token = bin2hex(random_bytes(60));
        $this->userid = $user;
        $this->expriteAt = new DateTime('+5 hour');
    }


    public function getExpriteAt(): ?\DateTimeInterface
    {
        return $this->expriteAt;
    }

    public function setExpriteAt(\DateTimeInterface $expriteAt): self
    {
        $this->expriteAt = $expriteAt;

        return $this;
    }

    public function getUserid(): ?User
    {
        return $this->userid;
    }

    public function setUserid(?User $userid): self
    {
        $this->userid = $userid;

        return $this;
    }

    public function addTime()
    {
        $this->setExpriteAt( new DateTime('+5 hour'));
    }

}
