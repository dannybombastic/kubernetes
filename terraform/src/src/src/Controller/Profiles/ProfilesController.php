<?php

namespace App\Controller\Profiles;

use App\Entity\CvRoute;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/profiles")
 * 
 */
class ProfilesController extends AbstractController
{
    /**
     * @Route("/{id}", name="profiles", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $text = str_replace('/profiles/','', $request->getRequestUri());

        return $this->render('profiles/index.html.twig', [
            'controller_name' => $text,
        ]);
    }

    /**
     * @Route("/cv/daniel", name="app_profiles_dani")
     */
    public function daniel(): Response
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findBy(['email' => 'dannybombastic@gmail.com'])[0];

        return $this->render('profiles/daniel/index.html.twig', [
            'controller_name' => 'Daniel',
            'user_info' => $user
        ]);
    }

    /**
     * @Route("/cv/ismael", name="app_profiles_isma")
     */
    public function ismael(): Response
    {
        return $this->render('profiles/ismael/index.html.twig', [
            'controller_name' => 'Ismael',
        ]);
    }

    /**
     * @Route("/cv/camacho", name="app_profiles_camacho")
     */
    public function camacho(): Response
    {
        return $this->render('profiles/camacho/index.html.twig', [
            'controller_name' => 'camacho',
        ]);
    }
}
