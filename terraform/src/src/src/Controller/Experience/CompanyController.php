<?php

namespace App\Controller\Experience;

use App\Entity\Company;
use App\Form\CompanyType;
use App\Repository\CompanyRepository;
use App\Repository\ExperienceRepository;
use App\Service\FileUploader;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/company")
 * @IsGranted("ROLE_USER")
 */
class CompanyController extends AbstractController
{
    /**
     * @Route("/", name="app_company_index", methods={"GET"})
     */
    public function index(CompanyRepository $companyRepository, Request $request, PaginatorInterface $paginator, ExperienceRepository $experienceRepository): Response
    {

        $q = $request->query->get('q');
        $user_id = $this->getUser()->getId();

        $queryBuilder = $companyRepository->getCompanyByUser($q, $user_id, $experienceRepository);

        $paginator = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1),
            5
        );
        dump($paginator);
        return $this->render('company/index.html.twig', [
            'pagination' => $paginator,
        ]);
    }

    /**
     * @Route("/new", name="app_company_new", methods={"GET","POST"})
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $company = new Company();
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $logo = $form->get('comlogo')->getData();
          
            if ($logo) {
                $fileName = $fileUploader->upload($logo);
                $company->setComlogo($fileName);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($company);
                $entityManager->flush();
                return $this->redirectToRoute('app_company_index');
            }


            
        }

        return $this->render('company/new.html.twig', [
            'company' => $company,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_company_show", methods={"GET"})
     */
    public function show(Company $company): Response
    {
        return $this->render('company/show.html.twig', [
            'company' => $company,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_company_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Company $company, FileUploader $fileUploader): Response
    {
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $logo = $form->get('comlogo')->getData();
            if ($logo) {
                $fileName = $fileUploader->upload($logo);
                $company->setComlogo($fileName);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('app_company_index');
        }

        return $this->render('company/edit.html.twig', [
            'company' => $company,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_company_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Company $company): Response
    {
        if ($this->isCsrfTokenValid('delete' . $company->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($company);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_company_index');
    }
}
