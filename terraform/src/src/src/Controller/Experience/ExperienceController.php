<?php

namespace App\Controller\Experience;

use App\Entity\Company;
use App\Entity\Experience;
use App\Form\CompanyType;
use App\Form\ExperienceType;
use App\Repository\ExperienceRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/experience")
 * @IsGranted("ROLE_USER")
 */
class ExperienceController extends AbstractController
{

    /**
     * @Route("/", name="app_experience_index", methods={"GET"})
     */
    public function index(ExperienceRepository $experienceRepository, Request $request, PaginatorInterface $paginator): Response
    {

        $q = $request->query->get('q');
        $user_id = $this->getUser()->getId();

        $queryBuilder = $experienceRepository->getWithShareQuerybuilder($q, $user_id);

        $paginator = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1),
            5
        );
        return $this->render('experience/index.html.twig', [
            'pagination' => $paginator
        ]);
    }

    /**
     * @Route("/new", name="experience_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {

        $experience = new Experience();
        $experience->setUserid($this->getUser());

        $form = $this->createForm(ExperienceType::class, $experience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($experience);
            $entityManager->flush();

            return $this->redirectToRoute('app_experience_index');
        }

        return $this->render('experience/new.html.twig', [
            'experience' => $experience,
            'form' => $form->createView()

        ]);
    }

    /**
     * @Route("/{id}", name="app_experience_show", methods={"GET"})
     * @IsGranted("OWNER", subject="experience")
     */
    public function show(Experience $experience): Response
    {
        return $this->render('experience/show.html.twig', [
            'experience' => $experience,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_experience_edit", methods={"GET","POST"})
     * @IsGranted("OWNER", subject="experience")
     */
    public function edit(Request $request, Experience $experience): Response
    {
        $form = $this->createForm(ExperienceType::class, $experience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('app_experience_index');
        }

        return $this->render('experience/edit.html.twig', [
            'experience' => $experience,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_experience_delete", methods={"DELETE"})
     * @IsGranted("OWNER", subject="experience")
     */
    public function delete(Request $request, Experience $experience): Response
    {
        if ($this->isCsrfTokenValid('delete' . $experience->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($experience);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_experience_index');
    }
}
