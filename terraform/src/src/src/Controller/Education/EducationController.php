<?php

namespace App\Controller\Education;

use App\Entity\Education;
use App\Form\EducationType;
use App\Repository\EducationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/education")
 * @IsGranted("ROLE_USER")
 */
class EducationController extends AbstractController
{
    /**
     * @Route("/", name="app_education_index", methods={"GET"})
     */
    public function index(EducationRepository $educationRepository): Response
    {
        $educations = $this->getUser()->getEducation();
        return $this->render('education/index.html.twig', [
            'education' => $educations,
        ]);
    }

    /**
     * @Route("/new", name="app_education_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $education = new Education();
        $education->setUserid($this->getUser());
        $form = $this->createForm(EducationType::class, $education);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($education);
            $entityManager->flush();

            return $this->redirectToRoute('app_education_index');
        }

        return $this->render('education/new.html.twig', [
            'education' => $education,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_education_show", methods={"GET"})
     * @IsGranted("OWNER", subject="education")
     */
    public function show(Education $education): Response
    {
        return $this->render('education/show.html.twig', [
            'education' => $education,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_education_edit", methods={"GET","POST"})
     * @IsGranted("OWNER", subject="education")
     */
    public function edit(Request $request, Education $education): Response
    {
        $form = $this->createForm(EducationType::class, $education);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('app_education_index');
        }

        return $this->render('education/edit.html.twig', [
            'education' => $education,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_education_delete", methods={"DELETE"})
     * @IsGranted("OWNER", subject="education")
     */
    public function delete(Request $request, Education $education): Response
    {
        if ($this->isCsrfTokenValid('delete' . $education->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($education);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_education_index');
    }
}
