<?php

namespace App\DataFixtures;



use App\DataFixtures\BaseFixture;
use App\Entity\Skill;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class SkillFixture extends BaseFixture implements DependentFixtureInterface
{
    public function loadData(ObjectManager $manager)
    {

        $this->createMany(Skill::class, 70, function (Skill $skill, $count) {
            // create 20 products! Bam!
            $skill->setTechname($this->faker->userName);
            $skill->setTechdescription($this->faker->realText($maxNbChars = 200, $indexSize = 2));
            $skill->setUserid($this->getReference(User::class . '_' . $this->faker->numberBetween(0, 19)));
            $skill->setTechpercent($this->faker->numberBetween($min = 100, $max = 100));
        });
        $manager->flush();
    }


    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
