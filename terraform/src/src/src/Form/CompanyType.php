<?php

namespace App\Form;

use App\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotNull;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $company = $options['data'] ?? null;
        $isEdit = $company->getComlogo();
        $message  = $isEdit;

        $imageConstraints[] = new Image([
            'maxSize' => '1024k',
            'mimeTypes' => [
                'image/jpeg',
                'image/png',
                'image/jpeg',
            ],
            'mimeTypesMessage' => 'Please upload a valid Img Format',
        ]);

        if (null === $isEdit) {
            $imageConstraints[] = new NotNull([
                'message' => 'Please upload an image',
            ]);
            $message = 'Select an Company image';
        }

        $builder
            ->add('name', TextType::class, [
                'label' => 'Company name'
            ])
            ->add('comdescription', TextareaType::class, [
                'label' => 'Company description',
                'attr' => ['rows' => 6]
            ])
            ->add('comlogo', FileType::class, [
                'attr' => [
                    'placeholder' => $message,
                ],
                'label' => 'Company logo',
                'required' => false,
                'mapped' => false,
                'constraints' => $imageConstraints,


            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
