<?php

namespace App\Form;

use App\Entity\GitRepositories;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GitRepositoriesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('githuburl', UrlType::class, [])
            ->add('gitlaburl', UrlType::class, [])
            ->add('gitlabaccesstoken', TextType::class, [])
            ->add('githubaccesstoken', TextType::class, [])
            ->add('usernamegitlab', TextType::class, [])
            ->add('usernamegithub', TextType::class, [])
            ->add('gitladid', TextType::class)
            ->add('gitlabispub', CheckboxType::class, [
                'required' => false,
            ])
            ->add('githubispub', CheckboxType::class, [
                'required' => false,
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GitRepositories::class,
        ]);
    }
}
