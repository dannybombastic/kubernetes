<?php

namespace App\Repository;

use App\Entity\GitRepositories;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GitRepositories|null find($id, $lockMode = null, $lockVersion = null)
 * @method GitRepositories|null findOneBy(array $criteria, array $orderBy = null)
 * @method GitRepositories[]    findAll()
 * @method GitRepositories[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GitRepositoriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GitRepositories::class);
    }

    // /**
    //  * @return GitRepositories[] Returns an array of GitRepositories objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GitRepositories
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
