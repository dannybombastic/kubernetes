resource "digitalocean_kubernetes_cluster" "portfolio" {
    name    = "portfolio"
    region  = "nyc1"
    version = "1.19.6-do.0"

    node_pool {
        name = "portfolio-nodes"
        size = "s-1vcpu-2gb"
        node_count = 1
    }

}