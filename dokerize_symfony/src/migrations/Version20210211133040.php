<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210211133040 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE api_token_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE company_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE education_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE experience_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE git_repositories_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE resume_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE skill_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE api_token (id INT NOT NULL, userid_id INT NOT NULL, token VARCHAR(255) NOT NULL, exprite_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7BA2F5EB58E0A285 ON api_token (userid_id)');
        $this->addSql('CREATE TABLE company (id INT NOT NULL, name VARCHAR(255) NOT NULL, comdescription VARCHAR(255) NOT NULL, comlogo VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE education (id INT NOT NULL, userid_id INT NOT NULL, school VARCHAR(255) NOT NULL, degree TEXT NOT NULL, degreedate DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DB0A5ED258E0A285 ON education (userid_id)');
        $this->addSql('CREATE TABLE experience (id INT NOT NULL, expcompany_id INT NOT NULL, userid_id INT NOT NULL, expname VARCHAR(255) NOT NULL, expdescription TEXT NOT NULL, expfrom DATE NOT NULL, expto DATE NOT NULL, createat DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_590C103B4DDF896 ON experience (expcompany_id)');
        $this->addSql('CREATE INDEX IDX_590C10358E0A285 ON experience (userid_id)');
        $this->addSql('CREATE TABLE git_repositories (id INT NOT NULL, userid_id INT NOT NULL, githuburl VARCHAR(255) DEFAULT NULL, gitlaburl VARCHAR(255) DEFAULT NULL, gitlabaccesstoken VARCHAR(255) DEFAULT NULL, githubaccesstoken VARCHAR(255) DEFAULT NULL, usernamegitlab VARCHAR(255) DEFAULT NULL, usernamegithub VARCHAR(255) DEFAULT NULL, gitlabispub BOOLEAN NOT NULL, githubispub BOOLEAN NOT NULL, githubjsn TEXT DEFAULT NULL, gitlabjsn TEXT DEFAULT NULL, gitladid VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_BD814B1F58E0A285 ON git_repositories (userid_id)');
        $this->addSql('COMMENT ON COLUMN git_repositories.githubjsn IS \'(DC2Type:array)\'');
        $this->addSql('COMMENT ON COLUMN git_repositories.gitlabjsn IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE resume (id INT NOT NULL, userid_id INT NOT NULL, name VARCHAR(255) NOT NULL, content TEXT NOT NULL, createat DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_60C1D0A058E0A285 ON resume (userid_id)');
        $this->addSql('CREATE TABLE skill (id INT NOT NULL, userid_id INT NOT NULL, techname VARCHAR(255) NOT NULL, techdescription VARCHAR(255) NOT NULL, techpercent INT NOT NULL, techasset VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5E3DE47758E0A285 ON skill (userid_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, linkedid_profile VARCHAR(255) DEFAULT NULL, infojob_profile VARCHAR(255) DEFAULT NULL, phonenumber VARCHAR(255) DEFAULT NULL, agreeterms DATE NOT NULL, name VARCHAR(255) NOT NULL, about TEXT DEFAULT NULL, photo VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('ALTER TABLE api_token ADD CONSTRAINT FK_7BA2F5EB58E0A285 FOREIGN KEY (userid_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE education ADD CONSTRAINT FK_DB0A5ED258E0A285 FOREIGN KEY (userid_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE experience ADD CONSTRAINT FK_590C103B4DDF896 FOREIGN KEY (expcompany_id) REFERENCES company (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE experience ADD CONSTRAINT FK_590C10358E0A285 FOREIGN KEY (userid_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE git_repositories ADD CONSTRAINT FK_BD814B1F58E0A285 FOREIGN KEY (userid_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE resume ADD CONSTRAINT FK_60C1D0A058E0A285 FOREIGN KEY (userid_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE skill ADD CONSTRAINT FK_5E3DE47758E0A285 FOREIGN KEY (userid_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE experience DROP CONSTRAINT FK_590C103B4DDF896');
        $this->addSql('ALTER TABLE api_token DROP CONSTRAINT FK_7BA2F5EB58E0A285');
        $this->addSql('ALTER TABLE education DROP CONSTRAINT FK_DB0A5ED258E0A285');
        $this->addSql('ALTER TABLE experience DROP CONSTRAINT FK_590C10358E0A285');
        $this->addSql('ALTER TABLE git_repositories DROP CONSTRAINT FK_BD814B1F58E0A285');
        $this->addSql('ALTER TABLE resume DROP CONSTRAINT FK_60C1D0A058E0A285');
        $this->addSql('ALTER TABLE skill DROP CONSTRAINT FK_5E3DE47758E0A285');
        $this->addSql('DROP SEQUENCE api_token_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE company_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE education_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE experience_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE git_repositories_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE resume_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE skill_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP TABLE api_token');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE education');
        $this->addSql('DROP TABLE experience');
        $this->addSql('DROP TABLE git_repositories');
        $this->addSql('DROP TABLE resume');
        $this->addSql('DROP TABLE skill');
        $this->addSql('DROP TABLE "user"');
    }
}
