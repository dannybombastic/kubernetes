<?php

namespace App\Repository;

use App\Repository\ExperienceRepository;
use App\Entity\Company;
use App\Entity\Experience;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Company|null find($id, $lockMode = null, $lockVersion = null)
 * @method Company|null findOneBy(array $criteria, array $orderBy = null)
 * @method Company[]    findAll()
 * @method Company[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Company::class);
    }

    // /**
    //  * @return Company[] Returns an array of Company objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function getWithShareQuerybuilder(?string $term, string $user_id): QueryBuilder
    {

        $qb = $this->createQueryBuilder('r')
            ->andWhere("r.userid = $user_id");

        if ($term) {
            $qb->andWhere('r.userid = :user AND r.expname LIKE :term OR r.expdescription LIKE :term')
                ->setParameter('term', '%' . $term . '%')
                ->setParameter('user', $user_id);
        }

        return $qb
            ->orderBy('r.createat', 'DESC');
    }

    public function getCompanyByUser($term, string $user_id, ExperienceRepository $experienceRepository)
    {
        return $experienceRepository->getCompanyByUser($term, $user_id);
    }
    /*
    public function findOneBySomeField($value): ?Company
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
