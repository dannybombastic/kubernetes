<?php

namespace App\Form;

use App\Entity\Skill;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\PositiveOrZero;

class SkillType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $skill = $options['data'] ?? null;
        $isEdit = $skill->getTechasset();
        $message  = $isEdit;

        $numberValidator = new PositiveOrZero([
            'message' => 'The number must be positive'
        ]);
        $imageConstraints[] = new Image([
            'maxSize' => '1024k',
            'mimeTypes' => [
                'image/jpeg',
                'image/png',
                'image/jpeg',
            ],
            'mimeTypesMessage' => 'Please upload a valid Img Format',
        ]);

        if (null === $isEdit) {
            $imageConstraints[] = $this->setValidatorMessage('Please upload an image');
            $message = 'Select an Skill image';
        }


        $builder
            ->add('techname', TextType::class, [
                'constraints' => $this->setValidatorMessage('name')
            ])
            ->add('techdescription', TextareaType::class, [
                'constraints' => $this->setValidatorMessage('description'),
                'attr'=> ['rows'=> 6]
            ])
            ->add('techpercent', NumberType::class, [
                'constraints' => $this->addListValidator($this->setValidatorMessage('percent'), [$numberValidator])
            ])
            ->add('techasset', FileType::class, [
                'attr' => [
                    'placeholder' => $message,
                ],
                'label' => 'imagen',
                'required' => false,
                'mapped' => false,
                'constraints' => $imageConstraints,


            ]);
    }

    public function setValidatorMessage(string $msg): ?NotNull
    {
        return  new NotNull([
            'message' => "You must fill the $msg",
        ]);
    }

    public function addListValidator($validator = null, array $list)
    {
        $list[] = $validator;
        return $list;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Skill::class,
        ]);
    }
}
