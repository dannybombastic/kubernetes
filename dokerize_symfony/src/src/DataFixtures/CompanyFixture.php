<?php

namespace App\DataFixtures;



use App\DataFixtures\BaseFixture;
use App\Entity\Company;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CompanyFixture extends BaseFixture implements DependentFixtureInterface
{
    public function loadData(ObjectManager $manager)
    {

        $this->createMany(Company::class, 20, function (Company $company, $count) {
            // create 20 products! Bam!
            $company->setName($this->faker->company);
            $company->setComdescription($this->faker->realText($maxNbChars = 200, $indexSize = 2));
            $company->setComlogo('vp-600c6ece34cb1.png');
        });
        $manager->flush();
    }


    public function getDependencies()
    {
        return [
            UserFixtures::class,
           
        ];
    }
}
