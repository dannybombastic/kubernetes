<?php

namespace App\DataFixtures;



use App\DataFixtures\BaseFixture;
use App\Entity\Company;
use App\Entity\Experience;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ExperienceFixture extends BaseFixture implements DependentFixtureInterface
{
    public function loadData(ObjectManager $manager)
    {

        $this->createMany(Experience::class, 50, function (Experience $experience, $count) {
            // create 20 products! Bam!
            $experience->setExpname($this->faker->jobTitle);
            $experience->setExpdescription($this->faker->realText($maxNbChars = 200, $indexSize = 2));
            $experience->setExpfrom($this->faker->dateTimeBetween('-100 days', '-1 days'));
            $experience->setExpto($this->faker->dateTimeBetween('-100 days', '-1 days'));
            $experience->setUserid($this->getReference(User::class . '_' . $this->faker->numberBetween(0, 19)));
            $experience->setExpCompany($this->getReference(Company::class . '_' . $this->faker->numberBetween(0, 19)));
        });
        $manager->flush();
    }


    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CompanyFixture::class
            
        ];
    }
}
