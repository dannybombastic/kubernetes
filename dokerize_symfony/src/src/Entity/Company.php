<?php

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 */
class Company
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $comdescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $comlogo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getComdescription(): ?string
    {
        return $this->comdescription;
    }

    public function setComdescription(string $comdescription): self
    {
        $this->comdescription = $comdescription;

        return $this;
    }

    public function getComlogo(): ?string
    {
        return $this->comlogo;
    }


    public function getComlogoUrl(): ?string
    {
        return 'assets/img/' . $this->comlogo;
    }

    public function setComlogo(?string $comlogo): self
    {
        $this->comlogo = $comlogo;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
