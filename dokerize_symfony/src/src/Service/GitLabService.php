<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\GitRepositoriesRepository;

class GitLabService
{

    const GITLAB_URL = 'https://gitlab.com/api/v4';
 

    const GITLAB_USER_PARAMS = [
        "user" => "/users/",
        "token" => "/projects?private_token="
    ];
 

    public GitRepositoriesRepository $gitRepository;

    public function __construct(GitRepositoriesRepository $gitRepository)
    {
        $this->gitRepository = $gitRepository;
    }

    public function saveGitlabInfo(User $userId): array
    {

        return $this->curlGitlab($userId);
    }

    public function curlGitlab(User $userId) : array
    {
        $gitlab = $this->gitRepository->findOneBy(["userid" => $userId->getId()]);
        $token = $gitlab->getGitlabaccesstoken();

        $url = self::GITLAB_URL
            . self::GITLAB_USER_PARAMS['user']
            . $gitlab->getGitladid()
            . self::GITLAB_USER_PARAMS['token']
            . $token;
      

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = curl_exec($ch);
        
        curl_close($ch);

        return json_decode($data, true);
    }
 
}

