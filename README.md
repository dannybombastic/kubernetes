- ### kubectl cli kubectl + cmd
  get po  -> pods
  get svc -> services
  get cm -> configMaps
  get ns -> namespaces
  get secrests -> sectrests
  get all -n  ns-name 

- ### delete
  delete + component [ pods, service ] --namespace name

- ### create
  create configmap config-name  --from-file=/path/to/file
  create sectret secret-name  --from-file=/path/to/file
  create namespace ns-name    
  
- ### ssh exec
  exec -it -n dev  dep-test-65b6f4b7b4-fcmxp -- sh 

- ### edit manifest
  edit configmap coredns --namespace kube-system

- ### describe component
  describe + component -n ns-name

- ### debugin
  cluster-info
  config view
  version --client
  api-versions
  api-resources


## configMap
  ```sh
    apiVersion: v1
    kind: ConfigMap
    metadata:
        name: nginx-config
        labels:
                app: nginx-cm
    data:
        nginx: |
                server {
                listen 80 default_server;
                listen [::]:80 default_server ipv6only=on;

                server_name localhost;
                root /var/www/public;
                index index.php index.html index.htm;

                location / {
                    try_files $uri $uri/ /index.php$is_args$args;
                }

                location ~ \.php$ {
                    try_files $uri /index.php =404;
                    fastcgi_pass php-upstream;
                    fastcgi_index index.php;
                    fastcgi_buffers 16 16k;
                    fastcgi_buffer_size 32k;
                    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                    fastcgi_read_timeout 600;
                    include fastcgi_params;
                }

                location ~ /\.ht {
                    deny all;
                }
                }
        nginx-confd: |
                upstream php-upstream {
                server
  ```
  ## Deployment
  ```sh
    apiVersion: apps/v1
    kind: Deployment
    metadata:
    namespace: dev
    annotations:
        kubernetes.io/change-cause: "Cambios v5"  # ayuda a passar a versiones anteriores o posteriores 
    name: dep-test
    labels:
        app: front
    template:  # pods
        metadata:
        labels:
            app: front
        spec:
        containers:
        - name: nginx
            image: nginx
            resources:
            limits:
                memory: "128Mi"
                cpu: "500m"
            ports:
            - containerPort: 9090


  ```



  ## Secrets
  ```sh
      apiVersion: v1
      kind: Secret
      metadata:
        name: db-secret
      type: Opaque
      data:  # stringData:
        password: $MY_password  

      #envsubst < secret.yam > tmp.yml

  ```

  ## Services

  ```sh
    apiVersion: v1
    kind: Service
    metadata:
    name: nginx-service
    labels:
        app: sev-label
    spec:
    type: NodePort  
    selector:
        app: nginx  <--- key pont select by label
    ports:
        - protocol: TCP
        port: 80
        targetPort: 8090  # what port i am goins to comsume from pods

  ```
  

  ### Pods
  ```sh
    apiVersion: v1
    kind: Pod
    metadata:
        name: myapp
        labels:
        name: myapp
    spec:
        containers:
        - name: myapp
        image: <Image>
        resources:
            limits:
            memory: "128Mi"
            cpu: "500m"
        ports:
            - containerPort: <Port>
  
  ```

  ### api-resources

  ```sh
NAME                              SHORTNAMES   APIVERSION                             NAMESPACED   KIND
bindings                                       v1                                     true         Binding
componentstatuses                 cs           v1                                     false        ComponentStatus
configmaps                        cm           v1                                     true         ConfigMap
endpoints                         ep           v1                                     true         Endpoints
events                            ev           v1                                     true         Event
limitranges                       limits       v1                                     true         LimitRange
namespaces                        ns           v1                                     false        Namespace
nodes                             no           v1                                     false        Node
persistentvolumeclaims            pvc          v1                                     true         PersistentVolumeClaim
persistentvolumes                 pv           v1                                     false        PersistentVolume
pods                              po           v1                                     true         Pod
podtemplates                                   v1                                     true         PodTemplate
replicationcontrollers            rc           v1                                     true         ReplicationController
resourcequotas                    quota        v1                                     true         ResourceQuota
secrets                                        v1                                     true         Secret
serviceaccounts                   sa           v1                                     true         ServiceAccount
services                          svc          v1                                     true         Service
mutatingwebhookconfigurations                  admissionregistration.k8s.io/v1        false        MutatingWebhookConfiguration
validatingwebhookconfigurations                admissionregistration.k8s.io/v1        false        ValidatingWebhookConfiguration
customresourcedefinitions         crd,crds     apiextensions.k8s.io/v1                false        CustomResourceDefinition
apiservices                                    apiregistration.k8s.io/v1              false        APIService
controllerrevisions                            apps/v1                                true         ControllerRevision
daemonsets                        ds           apps/v1                                true         DaemonSet
deployments                       deploy       apps/v1                                true         Deployment
replicasets                       rs           apps/v1                                true         ReplicaSet
statefulsets                      sts          apps/v1                                true         StatefulSet
tokenreviews                                   authentication.k8s.io/v1               false        TokenReview
localsubjectaccessreviews                      authorization.k8s.io/v1                true         LocalSubjectAccessReview
selfsubjectaccessreviews                       authorization.k8s.io/v1                false        SelfSubjectAccessReview
selfsubjectrulesreviews                        authorization.k8s.io/v1                false        SelfSubjectRulesReview
subjectaccessreviews                           authorization.k8s.io/v1                false        SubjectAccessReview
horizontalpodautoscalers          hpa          autoscaling/v1                         true         HorizontalPodAutoscaler
cronjobs                          cj           batch/v1beta1                          true         CronJob
jobs                                           batch/v1                               true         Job
certificatesigningrequests        csr          certificates.k8s.io/v1                 false        CertificateSigningRequest
leases                                         coordination.k8s.io/v1                 true         Lease
endpointslices                                 discovery.k8s.io/v1beta1               true         EndpointSlice
events                            ev           events.k8s.io/v1                       true         Event
ingresses                         ing          extensions/v1beta1                     true         Ingress
flowschemas                                    flowcontrol.apiserver.k8s.io/v1beta1   false        FlowSchema
prioritylevelconfigurations                    flowcontrol.apiserver.k8s.io/v1beta1   false        PriorityLevelConfiguration
ingressclasses                                 networking.k8s.io/v1                   false        IngressClass
ingresses                         ing          networking.k8s.io/v1                   true         Ingress
networkpolicies                   netpol       networking.k8s.io/v1                   true         NetworkPolicy
runtimeclasses                                 node.k8s.io/v1                         false        RuntimeClass
poddisruptionbudgets              pdb          policy/v1beta1                         true         PodDisruptionBudget
podsecuritypolicies               psp          policy/v1beta1                         false        PodSecurityPolicy
clusterrolebindings                            rbac.authorization.k8s.io/v1           false        ClusterRoleBinding
clusterroles                                   rbac.authorization.k8s.io/v1           false        ClusterRole
rolebindings                                   rbac.authorization.k8s.io/v1           true         RoleBinding
roles                                          rbac.authorization.k8s.io/v1           true         Role
priorityclasses                   pc           scheduling.k8s.io/v1                   false        PriorityClass
csidrivers                                     storage.k8s.io/v1                      false        CSIDriver
csinodes                                       storage.k8s.io/v1                      false        CSINode
storageclasses                    sc           storage.k8s.io/v1                      false        StorageClass
volumeattachments                              storage.k8s.io/v1                      false        VolumeAttachment


   

  ```

  ### subfolder

   
  ```sh
    cat ~/.kube/config 

    apiVersion: v1
    clusters:
    - cluster:
        certificate-authority: /home/dannybombastic/.minikube/ca.crt
        extensions:
        - extension:
            last-update: Sun, 21 Feb 2021 14:05:41 CET
            provider: minikube.sigs.k8s.io
            version: v1.17.1
        name: cluster_info
        server: https://192.168.100.164:8443
    name: minikube
    contexts:
    - context:
        cluster: minikube
        extensions:
        - extension:
            last-update: Sun, 21 Feb 2021 14:05:41 CET
            provider: minikube.sigs.k8s.io
            version: v1.17.1
        name: context_info
        namespace: default
        user: minikube
    name: minikube
    current-context: minikube
    kind: Config
    preferences: {}
    users:
    - name: minikube
    user:
        client-certificate: /home/dannybombastic/.minikube/profiles/minikube/client.crt
        client-key: /home/dannybombastic/.minikube/profiles/minikube/client.key

  ```

  ### volumen
  ```sh
    - emptyDir
    - hostPath
    - VC volumrn cloud
    - PV PVC Persitent Volune Claim
    - Reclaim Policy  - Retain  - Recycle - Delete

  ```


  ### terraform instalation

  ```sh

     - curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
     - sudo apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

     - sudo apt install terraform

     - TF_VAR_DIGITALOCEAN_TOKEN = token

     - terraform plan 

     - terraform apply

     - terraform destroy
 
  ``` 